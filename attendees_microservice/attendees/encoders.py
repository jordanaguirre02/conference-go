from .models import Attendee
from common.json import ModelEncoder
from .models import ConferenceVO, AccountVO


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = [
        "name",
        "import_href"
    ]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference"
        ]

    encoders = {
        "conference": ConferenceVODetailEncoder()
    }

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email).count()
        print(f"***** {count}")
        if count:
            return {"has_account": True}
        else:
            return {"has_account": False}
       # Get the count of AccountVO objects with email equal to o.email
        # Return a dictionary with "has_account": True if count > 0
        # Otherwise, return a dictionary with "has_account": False
