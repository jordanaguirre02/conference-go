from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()


from attendees.models import AccountVO


def UpdateAccountVO(ch, method, properties, body):
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    print("*****", content["updated"])
    updated_string = datetime.fromisoformat(content["updated"])
    updated = datetime.strftime(updated_string, '%Y-%m-%d-%H:%M:%S')
    if is_active:
        AccountVO.objects.update_or_create(
            email=email,
            updated=updated
        )
    else:
        AccountVO.objects.filter(email=email).delete()


while True:
    try:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='rabbitmq'))
        channel = connection.channel()
        channel.exchange_declare(
            exchange='account_info',
            exchange_type='fanout'
            )
        result = channel.queue_declare(queue='', exclusive=True)
        queue_name = result.method.queue
        channel.queue_bind(exchange='account_info', queue=queue_name)

        print(' [*] Waiting for logs. To exit press CTRL+C')
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=UpdateAccountVO,
            auto_ack=True
            )
        channel.start_consuming()
    except AMQPConnectionError:
        print("could not connect to RabbitMQ")
        time.sleep(10)
