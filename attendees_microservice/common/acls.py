import requests
from random import randint
from common.keys import PEXEL_KEY, WEATHER_DATA_KEY


def get_local_pic(city):
    page = requests.get(
            f"https://api.pexels.com/v1/search?query={city}",
            headers={"authorization": PEXEL_KEY[0]}
    )

    photos = page.json()["photos"]
    pic_url = photos[randint(1, len(photos)-1)]["url"]
    return pic_url


def get_weather_data(city, state):
    geo_params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": WEATHER_DATA_KEY[0]
    }

    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    try:
        geo_response = requests.get(geo_url, params=geo_params)
        geo_data = geo_response.json()
        latitude = geo_data[0]["lat"]
        longitude = geo_data[0]["lon"]
    except (KeyError, IndexError):
        return None

    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": WEATHER_DATA_KEY[0]
    }
    weather_url = "https://api.openweathermap.org/data/2.5/weather"

    weather_response = requests.get(weather_url, params=weather_params)
    weather_data = weather_response.json()
    try:
        description = weather_data["weather"][0]["description"]
        temp = weather_data["main"]["temp"]
    except (KeyError, IndexError):
        return None

    weather = {
        "description": description,
        "temp": temp
    }
    return weather
