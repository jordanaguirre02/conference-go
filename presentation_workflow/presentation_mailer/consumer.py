import json
import pika
import time
import django
import os
import sys
from pika.exceptions import AMQPConnectionError


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


from django.core.mail import send_mail


def process_approval(ch, method, properties, body):
    message = json.loads(body)
    name = message["presenter_name"]
    title = message["title"]
    email = message["presenter_email"]
    send_mail(
        'Your presentation has been accepted',
        f"{name}, we're happy to tell you that your presentation {title} has been accepted",
        'admin@conference.go',
        [f"{email}"],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    message = json.loads(body)
    name = message["presenter_name"]
    title = message["title"]
    email = message["presenter_email"]
    send_mail(
        'Your presentation has been rejected',
        f"{name}, we're sorry to tell you that your presentation {title} has been rejected",
        'admin@conference.go',
        [f"{email}"],
        fail_silently=False,
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel2 = connection.channel()
        channel.queue_declare(queue='approval')
        channel2.queue_declare(queue="rejection")
        channel.basic_consume(
            queue='approval',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel2.basic_consume(
            queue="rejection",
            on_message_callback=process_rejection,
            auto_ack=True
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("could not connect to RabbitMQ")
        time.sleep(10)
