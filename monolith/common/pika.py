import pika
import json


def pika_approve(data):
    body_data = json.dumps(data)
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="approval")
    channel.basic_publish(
        exchange="",
        routing_key="approval",
        body=body_data,
    )
    connection.close()


def pika_reject(data):
    body_data = json.dumps(data)
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="rejection")
    channel.basic_publish(
        exchange="",
        routing_key="rejection",
        body=body_data,
    )
    connection.close()
